<?php

/*
 |-------------------------------------------------------------------------
 | "Extractor" config for scaffolding.
 |-------------------------------------------------------------------------
 |
 | You can replace this conf file with config/amranidev/config.php
 | to let scaffold-interface interact with "Extractor".
 |
 */
return [

		'env' => [
        	'local',
    	],

		'package' => 'Extractor',

		'model' => base_path() . '/Walksoft/Extractor/src',

                'views' => base_path() . '/Walksoft/Extractor/resources/views',

                'controller' => base_path() . '/Walksoft/Extractor/src/Http/Controllers',

                'migration' => base_path() . '/Walksoft/Extractor/database/migrations',

		'database' => '/Walksoft/Extractor/database/migrations',

	   	'routes' => base_path() . '/Walksoft/Extractor/routes/web.php',

	   	'controllerNameSpace' => 'Walksoft\Extractor\\Http\\Controllers',

	   	'modelNameSpace' => 'Walksoft\Extractor',

		'loadViews' => 'Extractor',

	   ];
