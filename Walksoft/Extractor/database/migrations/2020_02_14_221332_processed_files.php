<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProcessedFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        Schema::create('processed_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file_name',255);
            $table->string('file_content',255);
            $table->text('extracted_content_json');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::dropIfExists('processed_files');
    }
}
