<?php

namespace Walksoft\Extractor;

use Illuminate\Database\Eloquent\Model; 

class ProcessedFile extends Model{
    
    protected  $table = 'processed_files';
    protected  $primaryKey = 'id';
    protected $casts = [
        'updated_at' => 'date:d/m/Y',
        'created_at' => 'date:d/m/Y',
        'extracted_content_json' => 'array'
    ];
    protected  $fillable = [
        'file_name',
        'file_content',
        'extracted_content_json'
    ];
    
}//ProcessedFiles
