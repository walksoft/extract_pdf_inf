<?php

namespace Walksoft\Extractor;

use Illuminate\Database\Eloquent\Model; 

class Configuration extends Model{
    
    protected  $table = 'configurations';
    protected  $primaryKey = 'id';
    protected $casts = [
        'updated_at' => 'date:d/m/Y',
        'created_at' => 'date:d/m/Y'
    ];
    protected  $fillable = [
        'parameter',
        'parameter_value'
    ];
    
}
