<?php

namespace walksoft\Extractor\Http\Controllers;

use ExtractorAppController as Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use Spatie\PdfToText\Pdf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ExtractorController extends Controller{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function getInfoPdfPath(Request $request){
        
        $Filesystem = new Filesystem();
        $respuesta = [];
        $infoFile = [];
        
        if($request->path && $Filesystem->exists($request->path) && $Filesystem->isFile($request->path)){
            
            $extension = File::extension($request->path);
            
            if(strtoupper($extension) =='PDF'){
                if(PHP_OS == 'WINNT'){
                    $content = (new Pdf('C:/poppler-0.68.0/bin/pdftotext.exe'))->setPdf($request->path)->setOptions(['layout','r 96','eol'])->addOptions(['eol','f 1'])->text();
                }//if
                else{
                    $content = (new Pdf())->setPdf($request->path)->setOptions(['layout','r 96','eol'])->addOptions(['eol','f 1'])->text();    
                }//else                
                //type format PDF
                $type = $this->getTypeFile($content);
                if($type){
                    //call function format
                    //info file
                    $infoFile['extension'] = $extension;
                    $infoFile['filename'] = File::name($request->path);
                    $infoFile['size'] = File::size($request->path);
                    $infoFile['path'] = $request->path;
                    $respuesta = call_user_func_array(array($this,'get_info_format_'.$type),array($content,$infoFile));
                }//if    
            }//if
            else{
                $respuesta['codeError'] = '010';
                $respuesta['msgError'] = 'ERROR 010 - The file is not a pdf';
            }//else
        }//if
        else{
          $respuesta['codeError'] = '020';
          $respuesta['msgError'] = 'ERROR 020 - File does not exist';
        }//else
        
        return $respuesta;
        
    }//getInfoPdfPath
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function getInfoPdf(){
        set_time_limit(0);
        $configuration = \Walksoft\Extractor\Configuration::find(1);
        $data = [];
        $infoFile = [];
        
        if(!$configuration->parameter_value){
            return $data;
        }//if
        $files = File::allFiles($configuration->parameter_value);
        
        if(count($files) == 0){
            return $data;
        }//if
        
        foreach($files as $file){
            set_time_limit(0);
            if(strtoupper($file->getExtension()) == 'PDF'){
                if(PHP_OS == 'WINNT'){
                    $content = (new Pdf('C:/poppler-0.68.0/bin/pdftotext.exe'))->setPdf(str_replace('/','\\',$file->getPathname()))->setOptions(['layout','r 96','eol'])->addOptions(['eol','f 1'])->text();
                }//if
                else{
                    $content = (new Pdf())->setPdf($file->getPathname())->setOptions(['layout','r 96','eol'])->addOptions(['eol','f 1'])->text();    
                }//else                
                //type format PDF
                $type = $this->getTypeFile($content);
                if($type){
                    //call function format
                    //info file
                    $infoFile['extension'] = $file->getExtension();
                    $infoFile['filename'] = $file->getFilename();
                    $infoFile['size'] = $file->getSize();
                    $infoFile['path'] = $file->getPathname();
                    $data[] = call_user_func_array(array($this,'get_info_format_'.$type),array($content,$infoFile));
                }//if
            }//if
        }//foreach

        return $data;
        
    }//getInfoPdf
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function getTypeFile($content){

        if(preg_match('/(^PORTEL\stramita\sla\spetición\sde\sescala\sa\sfecha\s:)/',$content) || preg_match('/(^Escala\sNº:)/',$content)){
            $type = 'DUE';
        }//if
        else if(preg_match('/(^AUTORIZADA\s+ENTRADA\s+SALIDA)/',$content) || preg_match('/(^SOLICITUD DECLARACIÓN DE ENTRADA)(\s+)(DE MERCANCÍAS PELIGROSAS)/',$content)){
            $type = 'DMP';
        }//else if
        else{
            $type = false;
        }//else
            
        return $type;
        
    }//getTypeFile
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function get_info_format_DUE($content,$file){
        
        //info file
        $data['extension'] = $file['extension'];
        $data['filename'] = $file['filename'] ;
        $data['size'] = $file['size'];

        preg_match('/(Escala Nº: )(.*?)(\s)/',$content,$matches);
        $data['escalaNo'] = trim($matches[2])?trim($matches[2]):NULL;
        
        preg_match('/(OMI:)(.*?)(Escala)/s',$content,$matches);
        $match = preg_replace('/[\r\n|\n|\r]+/','',$matches[2]);
        
        //información Buque
        preg_match('/BuqueNombre(.*?):Número\sIMO:/s',$match,$matches);
        $data['nombre'] = trim($matches[1])?trim($matches[1]):NULL;
                                
        preg_match('/Número\sIMO:(.*?)MMSI:/s',$match,$matches);
        $numero_imo = trim($matches[1])?trim($matches[1]):NULL;
        if(!$numero_imo && preg_match('/\s\d+$/s',$match)){
            preg_match('/\s\d+$/s',$match,$matches);
            $numero_imo = trim($matches[0])?trim($matches[0]):NULL;
        }//
        $data['numero_imo'] = $numero_imo;
           
        preg_match('/MMSI:(.*?)CallSign:/s',$match,$matches);
        $data['mmsi'] = trim($matches[1])?trim($matches[1]):NULL;
        
        preg_match('/CallSign:(.*?)Bandera:/s',$match,$matches);
        $data['callSign'] = trim($matches[1])?trim($matches[1]):NULL;
        
        preg_match('/Bandera:(\s+)?(.*?)(\s+|$)/s',$match,$matches);        
        $data['bandera'] = trim($matches[1])?trim($matches[1]):(trim($matches[2])?trim($matches[2]):NULL);
                
        //información Escala
        preg_match('/Bandera:.*Escala(.*?)Tripulación y pasajeros/s',$content,$matches);
        $match = preg_replace('/[\r\n|\n|\r]+/','',$matches[1]);
        
        preg_match('/^ETA:(.*?)Puerto anterior:/s',$match,$matches);
        $data['eta'] = trim($matches[1])?trim($matches[1]):NULL;
        
        preg_match('/ETD:(.*?)Puerto siguiente:/s',$match,$matches);
        $data['etd'] = trim($matches[1])?trim($matches[1]):NULL;
        
        preg_match('/Puerto anterior:(.*?)Servicio marítimo:/s',$match,$matches);
        $puerto_anterior = trim($matches[1])?trim($matches[1]):NULL;
        $data['puerto_anterior'] = preg_replace('/\s+/',' ',$puerto_anterior);
        
        preg_match('/Puerto siguiente:(.*?)ATA:/s',$match,$matches);
        $puerto_siguiente = trim($matches[1])?trim($matches[1]):NULL;
        $data['puerto_siguiente'] = preg_replace('/\s+/',' ',$puerto_siguiente);

        //información Tripulación y pasajeros
        preg_match('/Tripulación y pasajeros(.*?)\(Lista de tripulantes en el apéndice 2/s',$content,$matches);        
        $match = preg_replace('/[\r\n|\n|\r]+/','',$matches[1]);
        
        preg_match('/^A la entrada. Capitán:(.*?)Núm. Tripulantes:/s',$match,$matches);
        $data['a_la_entrada_capitan'] = trim($matches[1])?trim($matches[1]):NULL;
        
        preg_match('/A la salida. Capitán:(.*?)Núm. Tripulantes:/s',$match,$matches);
        $data['a_la_salida_capitan'] = trim($matches[1])?trim($matches[1]):NULL;
                          
        //información Estadía
        preg_match('/Estadía: atraque\/fondeo:(.*?)Detalle de operaciones tráfico mercantil/s',$content,$matches);         
        $match = preg_replace('/[\r\n|\n|\r]+/','',$matches[1]);
        
        preg_match('/Puesto de atraque\/fondeo:(.*?)Calado llegada:/s',$match,$matches);
        $data['puesto_atraque_fondeo'] = trim($matches[1])?trim($matches[1]):NULL;
          
        //información - Detalle de operaciones de tráfico mercantil
        preg_match('/Detalle de operaciones tráfico mercantil(.*?)Carga\/descarga/s',$content,$matches);
        preg_match('/Cía. estibadora(.*)/s',$matches[1],$match);        
        $detalles = preg_split('/\n/',$matches[1]);
        $data['detalle_op_traf_merc'] = $this->get_details_op_traf_mercantil_DUE($detalles);
        
        return $data;

    }//get_info_format_DUE
    
    public function get_details_op_traf_mercantil_DUE($detalles){  
        $cabezeras = $detalles[1];
        unset($detalles[0]); 
        unset($detalles[1]); 
        //die(var_dump($detalles));
        $data =[]; 
        $result =[];
        if(count($detalles)>0){
            $i = 0;
            foreach($detalles as $key => $detalle){
                
                if($detalle){

                    $init = strpos($cabezeras,'Tipo op.');
                    $end = strpos($cabezeras,'Pasaje/Carga');   
                    $tipo_op = trim(substr($detalle,$init,$end));
                    $data[$i]['tipo_op'] =  $tipo_op ? $tipo_op: NULL;

                    $init = strpos($cabezeras,'Pasaje/Carga');
                    $end = strpos($cabezeras,'Tipo ud.');
                    $pasaje_carga = trim(substr($detalle,$init,($end-$init)));
                    $data[$i]['pasaje_carga'] = $pasaje_carga ? $pasaje_carga : NULL;
                   
                    $init = strpos($cabezeras,'Tipo ud.');
                    $end = strpos($cabezeras,'Cantidad');
                    $tipo_ud = trim(substr($detalle,$init,($end-$init)));
                    $data[$i]['tipo_ud'] = $tipo_ud ? $tipo_ud : NULL;
 
                    $init = strpos($cabezeras,'Cantidad');
                    $end = strpos($cabezeras,'P.máx/ud.');
                    $cantidad = trim(substr($detalle,$init,($end-$init)));
                    $data[$i]['cantidad'] = $cantidad ? $cantidad : NULL;

                    $init = strpos($cabezeras,'P.máx/ud.');
                    $end = strpos($cabezeras,'Lugar des/pre-embarque')-1;
                    $p_max_ud = trim(substr($detalle,$init,($end-$init)));
                    $data[$i]['p_max_ud'] = $p_max_ud ? $p_max_ud : NULL;
                    
                    $init = strpos($cabezeras,'Lugar des/pre-embarque')-1;
                    $end = strpos($cabezeras,'Cía. estibadora')-1;
                    $lugar_des_pre_embarque = trim(substr($detalle,$init,($end-$init)));
                    $data[$i]['lugar_des_pre_embarque'] = $lugar_des_pre_embarque ? $lugar_des_pre_embarque : NULL;
                    
                    $init = strpos($cabezeras,'Cía. estibadora')-1;
                    $cia_estibadora = trim(substr($detalle,$init));
                    $data[$i]['cia_estibadora'] = $cia_estibadora ? $cia_estibadora : NULL;
                    
                    $i++;                    
                }//if
            }//foreach            
        }//if   
        if(count($data) > 0){
            foreach($data as $key => $info){
                if(!$info['tipo_op'] && !$info['pasaje_carga'] && !$info['tipo_ud'] && !$info['cantidad'] && !$info['p_max_ud'] && !$info['lugar_des_pre_embarque'] && !$info['cia_estibadora']){
                    unset($data[$key]);
                }//if
                else if(!$info['tipo_op'] && !$info['tipo_ud'] && !$info['cantidad']){                    
                    if($info['pasaje_carga'] && array_key_exists(($key-1),$data)){
                        $data[($key-1)]['pasaje_carga'] = $data[($key-1)]['pasaje_carga'].' '.$info['pasaje_carga'];
                    }//if
                    if($info['cia_estibadora'] && array_key_exists(($key-1),$data)){
                        $data[($key-1)]['cia_estibadora'] = $data[($key-1)]['cia_estibadora'].' '.$info['cia_estibadora'];
                    }//if
                    unset($data[$key]);
                }//else if
            }//foreach
            foreach($data as $key => $info){
               $result[] = $info;
            }//foreach
        }//if
        return $result;        
    }//get_info_format_DUE
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */    
    public function get_info_format_DMP($content,$file){

        //info file
        $data['extension'] = $file['extension'];
        $data['filename'] = $file['filename'] ;
        $data['size'] = $file['size'];
        $data['Presentacion'] = NULL;
        $data['granel_liquido'] = NULL;
        $data['granel_solido'] = NULL;
        $data['contenedor'] = NULL;
        $data['carga_general'] = NULL; 
        
        if(!Storage::exists('tmp')){
            Storage::makeDirectory('tmp',0755,true,true);
        }//if
        else{
            $Filesystem = new Filesystem();
            $Filesystem->cleanDirectory(storage_path().'/tmp');
        }//else        
        
        if(PHP_OS == 'WINNT'){
            $content = shell_exec('C:/poppler-0.68.0/bin/pdftohtml.exe '.(str_replace('\\','\\\\',str_replace('/','\\',$file['path']))).' '.str_replace('\\','\\\\',storage_path()).'\\app\\tmp\\result.pdf');
        }//if
        else{
            $content = shell_exec('/usr/bin/pdftohtml '.($file['path']).' '.storage_path().'/app/tmp/result.pdf');
        }//else
        
        if($content){
            if(PHP_OS == 'WINNT'){
                $data['Presentacion'] = $this->is_ckeck_promedioColorImagen(storage_path().'\\app\\tmp\\result.pdf-1_9.png');
                $data['granel_liquido'] = $this->is_ckeck_promedioColorImagen(storage_path().'\\app\\tmp\\result.pdf-1_10.png');
                $data['granel_solido'] = $this->is_ckeck_promedioColorImagen(storage_path().'\\app\\tmp\\result.pdf-1_11.png');
                $data['contenedor'] = $this->is_ckeck_promedioColorImagen(storage_path().'\\app\\tmp\\result.pdf-1_12.png');
                $data['carga_general'] = $this->is_ckeck_promedioColorImagen(storage_path().'\\app\\tmp\\result.pdf-1_13.png');
            }//if
            else{
                $data['Presentacion'] = $this->is_ckeck_promedioColorImagen(storage_path().'/app/tmp/result.pdf-1_9.png');
                $data['granel_liquido'] = $this->is_ckeck_promedioColorImagen(storage_path().'/app/tmp/result.pdf-1_10.png');
                $data['granel_solido'] = $this->is_ckeck_promedioColorImagen(storage_path().'/app/tmp/result.pdf-1_11.png');
                $data['contenedor'] = $this->is_ckeck_promedioColorImagen(storage_path().'/app/tmp/result.pdf-1_12.png');
                $data['carga_general'] = $this->is_ckeck_promedioColorImagen(storage_path().'/app/tmp/result.pdf-1_13.png');                
            }//else
        }//if   
        
        return $data;
        
    }//get_info_format_DUE
    
    public function is_ckeck_promedioColorImagen($rutaImagen){
        
	$finfo=finfo_open(FILEINFO_MIME_TYPE);
	$fileMime=finfo_file($finfo, $rutaImagen);
 
	// abrimos la imagen
        if($fileMime=="image/jpeg" || $fileMime=="image/pjpeg"){
            $imgId=imagecreatefromjpeg($rutaImagen);    
        }//if		
	elseif($fileMime=="image/gif"){
            $imgId=imagecreatefromgif($rutaImagen);        
        }//elseif
	elseif($fileMime=="image/png"){
            $imgId=imagecreatefrompng($rutaImagen);
        }//elseif
	else{
            return array(0,0,0);
        }//else
 
	$red=0;
	$green=0;
	$blue=0;
	$total=0;
 
	// Recorremos todos los valores horizontales
	for($x=0;$x<imagesx($imgId);$x++){
            // Recorremos todos los valores verticales
            for($y=0;$y<imagesy($imgId);$y++){
                // Obtenemos los valores red, green, blue de cada pixel de la imagen
                $rgb=imagecolorat($imgId,$x,$y); 
                // devuelve el indice de cada color
                $red+=($rgb >> 16) & 0xFF;
                $green+=($rgb >> 8) & 0xFF;
                $blue+=$rgb & 0xFF; 
                $total++;
            }//for
	}//for
        // devolvemos un array con el promedio de los colores en rojo, verde y azul
	$redPromedio = round($red/$total);
	$greenPromedio = round($green/$total);
	$bluePromedio = round($blue/$total); 	
        if(($redPromedio >= 148 &&  $redPromedio <= 152) && ($greenPromedio >= 173 && $greenPromedio<=177) && ($bluePromedio >= 194 && $bluePromedio <= 198)){
           return 'Si'; 
        }//if
        else{
            return 'No';
        }//else        
    }//promedioColorImagen

}//ExtractorController
